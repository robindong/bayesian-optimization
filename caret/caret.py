import numpy as np
import pandas as pd

from pycaret.classification import *


TRAIN_CSV_FILE = "/home/rdong/data/joe.csv"


class CaretTrainer:
    def __init__(self, debug=False, n_jobs=None):
        self._num_rows = 100 if debug else None
        #self._df = pd.read_csv(TRAIN_CSV_FILE, nrows=self._num_rows).drop(columns=["crn"])
        self._df = pd.read_csv(TRAIN_CSV_FILE, nrows=self._num_rows)

    def train(self):
        dataset = self._df
        data = dataset.sample(frac=0.8, random_state=1023)
        data_unseen = dataset.drop(data.index).reset_index(drop=True)
        data.reset_index(drop=True, inplace=True)

        clf = setup(data=data, target="open_flag", session_id=1023)
        compare_models(verbose = False)


if __name__ == "__main__":
    trainer = CaretTrainer(True)
    avg_auc = trainer.train()
    print("avg_auc:", avg_auc)
