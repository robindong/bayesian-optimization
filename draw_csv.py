import glob
import numpy as np
import matplotlib.pyplot as plt

from statistics import median, mean


def sort_by_mean(data, algos):
    lst = list(zip(data, algos))
    # Sort by mean of avg-auc
    result = sorted(lst, key=lambda pair: mean(pair[0]))
    data, algos = list(zip(*result))
    return data, algos


def draw(file_dict, title):
    plt.figure()
    ax = plt.subplot(111)

    data = []
    algos = []
    for algo, log_files in file_dict.items():
        total_array = []
        for file_name in log_files:
            array = []
            with open(file_name) as fp:
                for line in fp.readlines():
                    avg_auc = line.split(",")[1]
                    print(avg_auc)
                    if len(avg_auc) < 1 or avg_auc[0] != "0":
                        continue
                    avg_auc = float(avg_auc)
                    array.append(avg_auc)
            total_array += array
        data.append(total_array)
        algos.append(algo)

    print(data, algos)
    data, algos = sort_by_mean(data, algos)

    ax.set_title(title)
    ax.boxplot(data, labels=algos, showmeans=True, showfliers=False)
    plt.ylim(0.75, 0.9)
    plt.ylabel("AUC")
    plt.show()


def main(algos, title):
    file_dict = {}
    for algo in algos:
        log_files = glob.glob(f"{title}/*{algo}*.csv")
        file_dict[algo] = log_files
    draw(file_dict, title)


if __name__ == "__main__":
    main(["pbt", "hb", "ahb"], "8_cores")
    main(["pbt", "hb", "ahb"], "16_cores")
    main(["pbt", "hb", "ahb"], "32_cores")
