import numpy as np
import pandas as pd
import fastprogress

fastprogress.fastprogress.NO_BAR = True

from fastai.tabular import *
from fastai.callbacks import *


TRAIN_CSV_FILE = "/home/rdong/data/joe.csv"


class TabularTrainer:
    def __init__(self, debug=False, n_jobs=None):
        self._num_rows = 10000 if debug else None
        df = (
            pd.read_csv(TRAIN_CSV_FILE, nrows=self._num_rows)
            .fillna(0)
            .drop(columns=["crn"])
        )
        print("count:", df["ref_dt"].unique().shape[0])
        for col in [
            "ref_dt",
            "fw_start",
            "f01_mem_cvm_end_date",
            "f01_store_open_date",
        ]:
            add_datepart(df, col)
        self._df, _ = TabularTrainer.one_hot_encoder(df, False)

    @staticmethod
    def one_hot_encoder(df, nan_as_category=True):
        nrows = df.shape[0]
        original_columns = list(df.columns)
        categorical_columns = []
        for col in df.columns:
            if col == "open_flag":
                continue
            if df[col].dtype == "object" or df[col].dtype == "int64":
                unique_count = df[col].unique().shape[0]
                if unique_count <= (nrows / 2):
                    categorical_columns.append(col)
                else:
                    print("col:", col, df[col])
        df = pd.get_dummies(
            df, columns=categorical_columns, dummy_na=nan_as_category
        )
        new_columns = [c for c in df.columns if c not in original_columns]
        return df, new_columns

    def get_object_columns(self, df):
        results = []
        for col_name, col_type in zip(df.columns, df.dtypes):
            if (
                col_type != np.int64
                and col_type != np.float64
                and col_type != np.bool
            ):
                results.append(col_name)
        return results

    def train(self, args):
        procs = [FillMissing, Categorify, Normalize]

        fold = 5
        aucs = []
        for index in range(fold):
            part = len(self._df) // fold
            valid_idx = range(index * part, (index + 1) * part)

            dep_var = "open_flag"
            cat_cols = self.get_object_columns(self._df)

            data = TabularDataBunch.from_df(
                "./",
                self._df,
                dep_var,
                valid_idx=valid_idx,
                procs=procs,
                cat_names=cat_cols,
            )

            params = {
                "nodes": 64,
                "emb_drop": 0.1,
                "lr": 0.4,
            }
            if not args or len(args) > 0:
                params = {**params, **args}

            learn = tabular_learner(
                data,
                layers=[params["nodes"]],
                emb_drop=params["emb_drop"],
                metrics=[accuracy, AUROC()],
            )
            cb = SaveModelCallback(learn, monitor="auroc", name="best")
            escb = EarlyStoppingCallback(learn, monitor="auroc", patience=5)
            learn.fit_one_cycle(10, params["lr"], callbacks=[cb])
            aucs.append(cb.best)
        return sum(aucs) / len(aucs)


if __name__ == "__main__":
    trainer = TabularTrainer()
    avg_auc = trainer.train({"lr": 0.46, "emb_drop": 0.35, "nodes": 9})
    print("avg_auc:", avg_auc)
