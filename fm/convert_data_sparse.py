import numpy as np
import pandas as pd

HOMECREDIT_TRAIN_CSV_FILE = "../../data/application_train.csv"


class Convertor:
    def __init__(self, debug=False):
        num_rows = 10000 if debug else None
        self._df = pd.read_csv(HOMECREDIT_TRAIN_CSV_FILE, nrows=num_rows)
        #self._df = self._df.iloc[:, range(8)]

    def convert(self):
        object_columns = []
        for col, col_type in zip(self._df.columns, self._df.dtypes):
            if col_type != np.int64 and col_type != np.float64:
                object_columns.append(col)

        new_df = pd.get_dummies(
            self._df, columns=object_columns, dummy_na=True
        )
        new_df.rename(columns={"open_flag": "TARGET"}, inplace=True)
        new_df = new_df.fillna(0)
        feats = [col for col in new_df.columns if col not in ["TARGET"]]
        for _, row in new_df.iterrows():
            line = [str(int(row["TARGET"]))]
            for col in feats:
                line.append(str(row[col]))
            print(" ".join(line))


def main():
    conv = Convertor(True)
    conv.convert()


if __name__ == "__main__":
    main()
