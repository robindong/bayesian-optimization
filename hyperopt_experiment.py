import click
import train
import numpy as np

from hyperopt import fmin, tpe, atpe, anneal, rand, hp, STATUS_OK

integer_columns = {"n_estimators", "num_leaves", "max_depth", "min_child_samples"}
trainer = train.JoeTrainer(True)
log = open("hyperopt.log", "w")


def objective(args):
    new_args = {}
    for key, value in args.items():
        if key in integer_columns:
            new_args[key] = int(value)
        else:
            new_args[key] = value
    avg_auc = trainer.train(new_args)
    log.write(f"args: {new_args} avg_auc: {avg_auc}\n")
    log.flush()
    return {"loss": 1.0 - avg_auc, "status": STATUS_OK}


@click.command()
@click.option("--n_trials", default=50, help="Number of trials")
@click.option("--argo", default="tpe.suggest")
def main(n_trials, argo):
    space = {
        "n_estimators": hp.loguniform(
            "n_estimators", np.log(10), np.log(100000)
        ),
        "learning_rate": hp.loguniform(
            "learning_rate", np.log(0.0001), np.log(1.0)
        ),
        "num_leaves": hp.uniform("num_leaves", 2, 2000),
        "max_depth": hp.uniform("max_depth", 2, 200),
        "min_child_samples": hp.loguniform(
            "min_child_samples", np.log(1), np.log(10000)
        ),
        "reg_alpha": hp.loguniform("reg_alpha", np.log(0.001), np.log(10.0)),
        "reg_lambda": hp.loguniform("reg_lambda", np.log(0.001), np.log(10.0)),
    }

    best = fmin(objective, space=space, algo=eval(argo), max_evals=n_trials)
    print("best:", best)


if __name__ == "__main__":
    main()
