import click
import optuna
import train

integer_columns = {"n_estimators", "num_leaves", "max_depth"}
trainer = train.JoeTrainer(True)
log = open("optuna.log", "w")


def objective(trial):
    new_args = {
        "n_estimators": int(
            trial.suggest_loguniform("n_estimators", 10, 100000)
        ),
        "learning_rate": trial.suggest_loguniform("learning_rate", 0.0001, 1.0),
        "num_leaves": int(trial.suggest_uniform("num_leaves", 2, 2000)),
        "max_depth": int(trial.suggest_uniform("max_depth", 2, 200)),
        "min_child_samples": int(
            trial.suggest_loguniform("min_child_samples", 1, 10000)
        ),
        "reg_alpha": trial.suggest_loguniform("reg_alpha", 0.001, 10.0),
        "reg_lambda": trial.suggest_loguniform("reg_lambda", 0.001, 10.0),
    }
    avg_auc = trainer.train(new_args)
    log.write(f"args: {new_args} avg_auc: {avg_auc}\n")
    log.flush()
    return avg_auc


@click.command()
@click.option("--n_trials", default=50, help="Number of trials")
@click.option("--argo", default="optuna.samplers.TPESampler()")
def main(n_trials, argo):
    study = optuna.create_study(
        sampler=eval(argo),
        pruner=optuna.pruners.MedianPruner(n_warmup_steps=10),
        direction="maximize",
    )

    study.optimize(objective, n_trials=n_trials)
    print("best:", study.best_trial)


if __name__ == "__main__":
    main()
