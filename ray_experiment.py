import sys
import ray
import time
import train
import numpy as np

from ray import tune
from ray.tune.suggest.hyperopt import HyperOptSearch
from ray.tune.schedulers import (
    PopulationBasedTraining,
    AsyncHyperBandScheduler,
    HyperBandScheduler,
)
from hyperopt import hp

n_jobs = int(sys.argv[1])
integer_columns = {
    "n_estimators",
    "num_leaves",
    "max_depth",
    "min_child_samples",
}


def objective(args):
    trainer = train.JoeTrainer(True, n_jobs)
    new_args = {}
    for key, value in args.items():
        if key in integer_columns:
            new_args[key] = int(value)
        else:
            new_args[key] = value
    avg_auc = trainer.train(new_args)
    tune.track.log(mean_accuracy=avg_auc)


def main(index):
    space = {
        "n_estimators": hp.loguniform(
            "n_estimators", np.log(10), np.log(100000)
        ),
        "learning_rate": hp.loguniform(
            "learning_rate", np.log(0.0001), np.log(1.0)
        ),
        "num_leaves": hp.uniform("num_leaves", 2, 2000),
        "max_depth": hp.uniform("max_depth", 2, 200),
        "min_child_samples": hp.loguniform(
            "min_child_samples", np.log(1), np.log(10000)
        ),
        "reg_alpha": hp.loguniform("reg_alpha", np.log(0.001), np.log(10.0)),
        "reg_lambda": hp.loguniform("reg_lambda", np.log(0.001), np.log(10.0)),
    }

    pbt_scheduler = PopulationBasedTraining(
        time_attr="training_iteration",
        metric="mean_accuracy",
        mode="max",
        hyperparam_mutations={
            # distribution for resampling
            "lr": lambda: np.random.uniform(0.0001, 1),
            # allow perturbations within this set of categorical values
            "momentum": [0.8, 0.9, 0.99],
        },
    )
    ahb_scheduler = AsyncHyperBandScheduler(
        time_attr="training_iteration", metric="mean_accuracy", max_t=2,
    )
    hb_scheduler = HyperBandScheduler(
        time_attr="training_iteration", metric="mean_accuracy", max_t=2,
    )

    algo = HyperOptSearch(space, metric="mean_accuracy")
    for name, scheduler in zip(
        ["pbt", "ahb", "hb"], [pbt_scheduler, ahb_scheduler, hb_scheduler]
    ):
        begin = time.time()
        analysis = tune.run(
            objective,
            scheduler=scheduler,
            search_alg=algo,
            num_samples=10,
            resources_per_trial={"cpu": n_jobs},
        )
        enduration = time.time() - begin
        with open(f"{name}_{index}_time.txt", "w") as fp:
            fp.write(str(enduration))

        print("Best:", analysis.get_best_config(metric="mean_accuracy"))
        print("dataframe:", analysis.dataframe())
        analysis.dataframe().to_csv(f"{name}_{index}.csv")


if __name__ == "__main__":
    ray.init(
        memory=1000 * 1024 * 1024,
        object_store_memory=200 * 1024 * 1024,
        driver_object_store_memory=100 * 1024 * 1024,
    )
    for index in range(5):
        main(index)
