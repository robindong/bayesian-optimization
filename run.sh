for iter in `seq 1 10`; do
  for n_trials in 10; do
    for argo in "optuna.samplers.TPESampler()" "optuna.samplers.RandomSampler()"; do
      python3 optuna_experiment.py --n_trials=${n_trials} --argo=${argo}
      mv optuna.log optuna_${n_trials}_${argo}_${iter}.log
    done
    for argo in "tpe.suggest" "atpe.suggest" "anneal.suggest" "rand.suggest"; do
      python3 hyperopt_experiment.py --n_trials=${n_trials} --argo=${argo}
      mv hyperopt.log hyperopt_${n_trials}_${argo}_${iter}.log
    done
  done
done
