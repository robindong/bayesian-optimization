rm -rf *.txt *.csv
rm -rf *_cores/

for n_jobs in 8 16 32; do
  rm -rf ${n_jobs}_cores/
  mkdir ${n_jobs}_cores/
  rm -rf *.csv *.txt
  python3 ray_experiment.py ${n_jobs}
  mv *.csv *.txt ${n_jobs}_cores/
done
