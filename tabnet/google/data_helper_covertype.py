# coding=utf-8 # Copyright 2020 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Data helper function for the Forest Covertype dataset."""

import tensorflow as tf

# Dataset size
# N_TRAIN_SAMPLES = 309871
N_VAL_SAMPLES = 923641 // 10
N_TEST_SAMPLES = 431014 // 10
NUM_FEATURES = 115
NUM_CLASSES = 2

# All feature columns in the data
LABEL_COLUMN = "target"
WEIGHT_COLUMN = "sample_weights"

BOOL_COLUMNS = []

INT_COLUMNS = []

STR_COLUMNS = ["f0_campaign_type", "f01_grouparea_code", "f01_mem_lifestsge_current", "f01_mem_segment_cell_previous", "f01_mem_segment_cvm", "f01_store_cluster_desc", "f04_scn_trn_weeks_shopped_8w", "f21_num_comp_within_5km", "f25_sg_redeem_times", "f27_email_mp_8w", "f27_os_mp_8w", "f0_multiplier"]

STR_NUNIQUESS = [4, 11, 9, 11, 11, 10, 10, 11, 11, 11, 11, 3]

FLOAT_COLUMNS = ["f01_mem_8wk_spent", "f01_mem_TENURE", "f01_mem_age", "f01_mem_bpm_tenure", "f02_flx_dsct_othr_wowd_amt", "f02_flx_dsct_othr_wowd_earn_rt", "f02_flx_dsct_wowd_all_amt", "f02_flx_dsct_wowd_all_earn_rt", "f02_scn_dsct_comb_rec_rt_ea", "f02_scn_dsct_othr_wowd_amt", "f02_scn_dsct_othr_wowd_earn_rt", "f02_scn_dsct_wowd_all_amt", "f02_scn_dsct_wowd_all_earn_rt", "f02_scn_slope_spend_8w", "f02_scn_wke_txn_no", "f03_flx_trn_ctgry_spend_pi_4556", "f03_scn_trn_ctgry_spend_pi_4556", "f04_scn_trn_days_bw_1st_last_shop_8w", "f04_scn_trn_days_shopped_8w", "f04_scn_trn_days_since_last_shop_8w", "f04_scn_trn_pcntl_basket_size_8w", "f04_scn_trn_tot_baskets_q_8w", "f06_scn_avg_wkly_spend_post4w_ly", "f06_scn_est_spend_yoy", "f06_scn_gowth_yoy", "f06_scn_grow_yoy_8wks_med", "f06_scn_sum_spend_8w_ly", "f07_et_activated_cnt_8w", "f07_et_activated_rate_8w", "f07_et_click_cnt_8w", "f07_et_click_rate_8w", "f07_et_no_activable_camp_8w", "f07_et_no_rdmable_camp_8w", "f07_et_open_cnt_8w", "f07_et_open_rate_8w", "f07_et_redeemed_rate_8w", "f07_et_sent_cnt_8w", "f07_et_sent_rate_8w", "f07_scan_rate_8w", "f07_swipe_bskt_cnt_8w", "f09_flx_trn_subctgry_spend_p_1502122", "f13_flx_sup_wk5_spend", "f13_scn_est_wtrecency_spend", "f13_scn_med_spend_12w", "f13_scn_sup_est_wtrecency_spend", "f13_scn_sup_wk1_spend", "f13_scn_wk1_spend", "f13_scn_wk1_spend_noise", "f13_scn_wk2_spend", "f13_scn_wk2_spend_noise", "f23_camp_activate_rate_send_8w", "f23_camp_open_rate_send_8w", "f23_camp_redeem_rate_send_8w", "f23_camp_redeem_rate_send_idx", "f23_camp_redeem_times_idx", "f23_cat_last_camp_hr_open", "f23_cna_camp_send_cnt_8w", "f23_cvm_avg_hr_to_open_8w", "f23_cvm_camp_redeem_rate_send_8w", "f23_cvm_camp_redeem_rate_send_idx", "f23_cvm_camp_send_cnt_8w", "f23_cvm_last_camp_hr_open", "f23_open_wkday_cnt_8w", "f23_open_wkend_cnt_8w", "f23_osp_last_camp_hr_open", "f24_scn_hhld_lylty_basket_8wk", "f25_avg_sales_supers_ex_open", "f25_avg_sales_supers_lylty_ex_open", "f25_avg_sales_supers_open", "f25_avg_supers_lylty_sales_ex_hurdle", "f25_avg_supers_lylty_sales_ex_reward", "f25_avg_supers_lylty_sales_open", "f25_avg_supers_lylty_sales_reward", "f25_avg_supers_sales_reward", "f25_mod_std_supers_lylty_sales_ex_hurdle", "f25_mod_std_supers_lylty_sales_ex_open", "f25_mod_std_supers_lylty_sales_reward", "f25_mod_std_supers_sales_ex_hurdle", "f25_mod_std_supers_sales_ex_open", "f25_mod_std_supers_sales_reward", "f25_sg_avg_redeem_reward", "f25_sg_click_cnt", "f25_sg_days_since_redeem", "f25_sg_days_since_target", "f25_sg_max_hurdle", "f25_sg_max_redeem_reward", "f25_sg_max_reward", "f25_sg_min_failed_hurdle", "f25_sg_min_hurdle", "f25_sg_redeem_cnt", "f25_sg_redeem_rt", "f25_sg_sent_cnt", "f25_sg_total_reward", "f25_std_supers_lylty_ratio_reward", "f25_std_supers_lylty_sales_ex_hurdle", "f25_std_supers_lylty_sales_reward", "f25_std_supers_sales_reward", "f26_scn_prod_cnt_8w_p_food_fresh", "f27_browser_mp_pct_8w", "f0_reward", "f0_spend_hurdle", "f03_flx_trn_ctgry_spend_pi_0576", "f03_flx_trn_ctgry_spend_pi_0585"]

DEFAULTS = ([[0] for col in INT_COLUMNS] + [[""] for col in BOOL_COLUMNS] +
            [[""] for col in STR_COLUMNS] + [[0.0] for col in FLOAT_COLUMNS] +
            [[-1]] + [[0.0]])

FEATURE_COLUMNS = (
    INT_COLUMNS + BOOL_COLUMNS + STR_COLUMNS + FLOAT_COLUMNS)
ALL_COLUMNS = FEATURE_COLUMNS + [LABEL_COLUMN] + [WEIGHT_COLUMN]


def get_columns():
  """Get the representations for all input columns."""

  columns = []
  if FLOAT_COLUMNS:
    columns += [tf.feature_column.numeric_column(ci) for ci in FLOAT_COLUMNS]
  if INT_COLUMNS:
    columns += [tf.feature_column.numeric_column(ci) for ci in INT_COLUMNS]
  if STR_COLUMNS:
    # pylint: disable=g-complex-comprehension
    columns += [
        tf.feature_column.embedding_column(
            tf.feature_column.categorical_column_with_hash_bucket(
                ci, hash_bucket_size=int(3 * num)),
            dimension=1) for ci, num in zip(STR_COLUMNS, STR_NUNIQUESS)
    ]
  if BOOL_COLUMNS:
    # pylint: disable=g-complex-comprehension
    columns += [
        tf.feature_column.embedding_column(
            tf.feature_column.categorical_column_with_hash_bucket(
                ci, hash_bucket_size=3),
            dimension=1) for ci in BOOL_COLUMNS
    ]
  return columns


def parse_csv(value_column):
  """Parses a CSV file based on the provided column types."""
  columns = tf.decode_csv(value_column, record_defaults=DEFAULTS)
  features = dict(zip(ALL_COLUMNS, columns))
  sample_weights = features.pop(WEIGHT_COLUMN)
  label = features.pop(LABEL_COLUMN)
  classes = tf.cast(label, tf.int32) - 1
  return features, classes, sample_weights


def input_fn(data_file,
             num_epochs,
             shuffle,
             batch_size,
             n_buffer=50,
             n_parallel=16):
  """Function to read the input file and return the dataset.

  Args:
    data_file: Name of the file.
    num_epochs: Number of epochs.
    shuffle: Whether to shuffle the data.
    batch_size: Batch size.
    n_buffer: Buffer size.
    n_parallel: Number of cores for multi-core processing option.

  Returns:
    The Tensorflow dataset.
  """

  # Extract lines from input files using the Dataset API.
  dataset = tf.data.TextLineDataset(data_file)

  if shuffle:
    dataset = dataset.shuffle(buffer_size=n_buffer)

  dataset = dataset.batch(batch_size, drop_remainder=True)
  dataset = dataset.map(parse_csv, num_parallel_calls=n_parallel)

  # Repeat after shuffling, to prevent separate epochs from blending together.
  dataset = dataset.repeat(num_epochs)
  return dataset
