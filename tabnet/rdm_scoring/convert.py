import pandas as pd


def pretty_print(columns):
    print("[" + ", ".join([f'"{col}"' for col in columns]) + "]")
  

df = pd.read_parquet("rdm_scored_M.parquet")

#col = df.pop("target")
#df.insert(0, col.name, col.astype("int32"))

del df["crn"]
del df["ref_dt"]
del df["pred"]
#del df["sample_weight"]
print(df)

str_columns = []
str_nuniques = []
float_columns = []

for col,type in zip(df.columns, df.dtypes):
    if col == "target" or col == "sample_weight":
        continue
    if type == "float32" or type == "float64":
        float_columns.append(col)
        values = df[col]
        df[col] = (values - values.min()) / (values.max() - values.min())
    else:
        str_columns.append(col)
        str_nuniques.append(df[col].nunique())

new_columns = str_columns + float_columns + ["target"] + ["sample_weight"]
df["target"] = df["target"].astype("int32") + 1
df = df.reindex(new_columns, axis=1)
print(df)

pretty_print(str_columns)
print(str_nuniques)
pretty_print(float_columns)

length = df.shape[0] // 10
#df.to_csv("private/rdm_all_features.csv", index=False, header=False)
# Export data
df.iloc[:2*length].to_csv("private/rdm_scoring_test.csv", index=False, header=False)
df.iloc[2*length:].to_csv("private/rdm_scoring_train.csv", index=False, header=False)
