import gc
import abc
import torch
import numpy as np
import pandas as pd

from pytorch_tabnet.tab_model import TabNetClassifier
from lightgbm import LGBMClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import KFold, StratifiedKFold

HOMECREDIT_TRAIN_CSV_FILE = "../../data/application_train.csv"
HOMECREDIT_TEST_CSV_FILE = "../../data/application_test.csv"

# From 'gs://wx-personal/joe/a02_decision-engine/isd/data/w_cmd_smpl/part-00000-468faef6-362e-4c8d-8375-f5c04651c071-c000.snappy.parquet'
JOE_TRAIN_CSV_FILE = "/home/rdong/data/joe.csv"


class Trainer(abc.ABC):
    def __init__(self, debug=False, n_jobs=8):
        self._num_rows = 200000 if debug else None
        self._data = self.get_data()
        self._n_jobs = n_jobs

    @staticmethod
    def one_hot_encoder(df, nan_as_category=True):
        original_columns = list(df.columns)
        categorical_columns = [
            col for col in df.columns if df[col].dtype == "object"
        ]
        df = pd.get_dummies(
            df, columns=categorical_columns, dummy_na=nan_as_category
        )
        new_columns = [c for c in df.columns if c not in original_columns]
        return df, new_columns

    def kfold_lightgbm(self, df, num_folds, stratified=False, args=None):
        # Divide in training/validation and test data
        train_df = df[df["TARGET"].notnull()]
        train_df.columns = [
            "".join(col if col.isalnum() else "_" for col in str(x))
            for x in train_df.columns
        ]
        train_df.reset_index(drop=True)
        test_df = df[df["TARGET"].isnull()]
        test_df.columns = [
            "".join(col if col.isalnum() else "_" for col in str(x))
            for x in test_df.columns
        ]
        test_df.reset_index(drop=True)

        print("train_df:", train_df.shape)
        train_df["MySet"] = np.random.choice(["train", "valid"], p=[.8, .2], size=(train_df.shape[0],))
        train_indices = train_df[train_df.MySet=="train"].index
        valid_indices = train_df[train_df.MySet=="valid"].index

        nunique = train_df.nunique()
        types = train_df.dtypes

        categorical_columns = []
        categorical_dims =  {}
        for col in train_df.columns:
            if types[col] == 'object' or nunique[col] < 200:
                print(col, train_df[col].nunique())
                l_enc = LabelEncoder()
                train_df[col] = train_df[col].fillna("VV_likely")
                train_df[col] = l_enc.fit_transform(train_df[col].values)
                categorical_columns.append(col)
                categorical_dims[col] = len(l_enc.classes_)
            else:
                train_df.fillna(train_df.loc[train_indices, col].mean(), inplace=True)
        print("train_indices:", train_indices)
        print("train_df index:", train_df.index)


        # Create arrays and dataframes to store results
        oof_preds = np.zeros(train_df.shape[0])
        sub_preds = np.zeros(test_df.shape[0])
        feats = [
            f
            for f in train_df.columns
            if f
            not in [
                "TARGET",
                "SK_ID_CURR",
                "SK_ID_BUREAU",
                "SK_ID_PREV",
                "index",
            ]
        ]

        cat_idxs = [ i for i, f in enumerate(feats) if f in categorical_columns]
        cat_dims = [ categorical_dims[f] for i, f in enumerate(feats) if f in categorical_columns]

        sum_valid_auc = 0.0

        for index in range(1):
            X_train = train_df[feats].loc[train_indices].values
            y_train = train_df["TARGET"].loc[train_indices].values

            X_valid = train_df[feats].loc[valid_indices].values
            y_valid = train_df["TARGET"].loc[valid_indices].values


            clf = TabNetClassifier(cat_idxs=cat_idxs,
                    cat_dims=cat_dims,
                    cat_emb_dim=16,
                    optimizer_fn=torch.optim.SGD,
                    optimizer_params=dict(lr=1e-1),
                    scheduler_params={"step_size": 50, "gamma": 0.9},
                    scheduler_fn=torch.optim.lr_scheduler.StepLR,
                    device_name="cpu",
                    mask_type="entmax"
                    )

            clf.fit(
                X_train,
                y_train,
                X_valid,
                y_valid,
                max_epochs=100,
                patience=20,
                batch_size=8192,
                num_workers=0,
                weights=1,
                drop_last=False
            )

            oof_preds[valid_indices] = clf.predict(
                X_valid, num_iteration=clf.best_iteration_
            )[:, 1]
            sub_preds += (
                clf.predict(
                    test_df[feats], num_iteration=clf.best_iteration_
                )[:, 1]
                / folds.n_splits
            )

            valid_auc = roc_auc_score(valid_y, oof_preds[valid_idx])
            sum_valid_auc += valid_auc

        avg_auc = sum_valid_auc / num_folds
        return avg_auc

    def train(self, args):
        avg_auc = self.kfold_lightgbm(
            self._data, num_folds=5, stratified=False, args=args
        )
        return avg_auc

    @abc.abstractmethod
    def get_data(self):
        pass


class HomeCreditTrainer(Trainer):
    def __init__(self, debug=False):
        Trainer.__init__(self, debug)

    def get_data(self):
        return HomeCreditTrainer.application_train_test(self._num_rows)

    @staticmethod
    def application_train_test(num_rows=None, nan_as_category=False):
        # Read data and merge
        df = pd.read_csv(HOMECREDIT_TRAIN_CSV_FILE, nrows=num_rows)
        test_df = pd.read_csv(HOMECREDIT_TEST_CSV_FILE, nrows=num_rows)
        df = df.append(test_df).reset_index()
        # Optional: Remove 4 applications with XNA CODE_GENDER (train set)
        df = df[df["CODE_GENDER"] != "XNA"]

        # Categorical features with Binary encode (0 or 1; two categories)
        for bin_feature in ["CODE_GENDER", "FLAG_OWN_CAR", "FLAG_OWN_REALTY"]:
            df[bin_feature], uniques = pd.factorize(df[bin_feature])
        # Categorical features with One-Hot encode
        df, cat_cols = Trainer.one_hot_encoder(df, nan_as_category)

        # NaN values for DAYS_EMPLOYED: 365.243 -> nan
        df["DAYS_EMPLOYED"].replace(365243, np.nan, inplace=True)
        # Some simple new features (percentages)
        df["DAYS_EMPLOYED_PERC"] = df["DAYS_EMPLOYED"] / df["DAYS_BIRTH"]
        df["INCOME_CREDIT_PERC"] = df["AMT_INCOME_TOTAL"] / df["AMT_CREDIT"]
        df["INCOME_PER_PERSON"] = (
            df["AMT_INCOME_TOTAL"] / df["CNT_FAM_MEMBERS"]
        )
        df["ANNUITY_INCOME_PERC"] = df["AMT_ANNUITY"] / df["AMT_INCOME_TOTAL"]
        df["PAYMENT_RATE"] = df["AMT_ANNUITY"] / df["AMT_CREDIT"]
        print("columns:", len(df.columns))
        return df


class JoeTrainer(Trainer):
    def __init__(self, debug=False, n_jobs=8):
        Trainer.__init__(self, debug, n_jobs)

    def get_data(self):
        # Read data
        df = pd.read_csv(JOE_TRAIN_CSV_FILE, nrows=self._num_rows)

        """# Categorical features with Binary encode
        for bin_feature in [
            "f01_mem_gender",
            "activate_flag",
            "unsub_flag",
            "redeem_flag",
        ]:
            df[bin_feature] = pd.factorize(df[bin_feature])"""
        # Categorical features with One-Hot encode
        df, cat_cols = Trainer.one_hot_encoder(df, False)
        df.rename(columns={"open_flag": "TARGET"}, inplace=True)
        # Set 20% of data as test set
        sample = df.sample(frac=0.2, random_state=1023)
        df.loc[sample.index, "TARGET"] = None
        gc.collect()
        return df


if __name__ == "__main__":
    trainer = JoeTrainer(debug=False)
    trainer.train({"max_depth": 2})
