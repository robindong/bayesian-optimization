import gc
import abc
import numpy as np
import pandas as pd

from lightgbm import LGBMClassifier
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import KFold, StratifiedKFold

HOMECREDIT_TRAIN_CSV_FILE = "../data/application_train.csv"
HOMECREDIT_TEST_CSV_FILE = "../data/application_test.csv"

# From 'gs://wx-personal/joe/a02_decision-engine/isd/data/w_cmd_smpl/part-00000-468faef6-362e-4c8d-8375-f5c04651c071-c000.snappy.parquet'
JOE_TRAIN_CSV_FILE = "/home/rdong/data/joe.csv"


class Trainer(abc.ABC):
    def __init__(self, debug=False, n_jobs=8):
        self._num_rows = 10000 if debug else None
        self._data = self.get_data()
        self._n_jobs = n_jobs

    @staticmethod
    def one_hot_encoder(df, nan_as_category=True):
        original_columns = list(df.columns)
        categorical_columns = [
            col for col in df.columns if df[col].dtype == "object"
        ]
        df = pd.get_dummies(
            df, columns=categorical_columns, dummy_na=nan_as_category
        )
        new_columns = [c for c in df.columns if c not in original_columns]
        return df, new_columns

    def kfold_lightgbm(self, df, num_folds, stratified=False, args=None):
        # Divide in training/validation and test data
        train_df = df[df["TARGET"].notnull()]
        train_df.columns = [
            "".join(col if col.isalnum() else "_" for col in str(x))
            for x in train_df.columns
        ]
        test_df = df[df["TARGET"].isnull()]
        test_df.columns = [
            "".join(col if col.isalnum() else "_" for col in str(x))
            for x in test_df.columns
        ]
        del df
        gc.collect()
        # Cross validation model
        if stratified:
            folds = StratifiedKFold(
                n_splits=num_folds, shuffle=True, random_state=1001
            )
        else:
            folds = KFold(n_splits=num_folds, shuffle=True, random_state=1001)
        # Create arrays and dataframes to store results
        oof_preds = np.zeros(train_df.shape[0])
        sub_preds = np.zeros(test_df.shape[0])
        feats = [
            f
            for f in train_df.columns
            if f
            not in [
                "TARGET",
                "SK_ID_CURR",
                "SK_ID_BUREAU",
                "SK_ID_PREV",
                "index",
            ]
        ]

        sum_valid_auc = 0.0

        for n_fold, (train_idx, valid_idx) in enumerate(
            folds.split(train_df[feats], train_df["TARGET"])
        ):
            train_x, train_y = (
                train_df[feats].iloc[train_idx],
                train_df["TARGET"].iloc[train_idx],
            )
            valid_x, valid_y = (
                train_df[feats].iloc[valid_idx],
                train_df["TARGET"].iloc[valid_idx],
            )

            # LightGBM parameters found by Bayesian optimization
            params = {
                "n_jobs": self._n_jobs,
                "n_estimators": 10000,
                "learning_rate": 0.02,
                "num_leaves": 34,
                "colsample_bytree": 0.9497036,
                "subsample": 0.8715623,
                "max_depth": 8,
                "reg_alpha": 0.041545473,
                "reg_lambda": 0.0735294,
                "min_split_gain": 0.0222415,
                "min_child_weight": 39.3259775,
                "silent": -1,
                "verbose": -1,
            }
            if not args or len(args) > 0:
                params = {**params, **args}

            clf = LGBMClassifier(**params)

            clf.fit(
                train_x,
                train_y,
                eval_set=[(valid_x, valid_y)],
                eval_metric="auc",
                verbose=200,
                early_stopping_rounds=200,
            )

            oof_preds[valid_idx] = clf.predict_proba(
                valid_x, num_iteration=clf.best_iteration_
            )[:, 1]
            sub_preds += (
                clf.predict_proba(
                    test_df[feats], num_iteration=clf.best_iteration_
                )[:, 1]
                / folds.n_splits
            )

            valid_auc = roc_auc_score(valid_y, oof_preds[valid_idx])
            sum_valid_auc += valid_auc
            del clf, train_x, train_y, valid_x, valid_y
            gc.collect()

        avg_auc = sum_valid_auc / num_folds
        return avg_auc

    def train(self, args):
        avg_auc = self.kfold_lightgbm(
            self._data, num_folds=5, stratified=False, args=args
        )
        return avg_auc

    @abc.abstractmethod
    def get_data(self):
        pass


class HomeCreditTrainer(Trainer):
    def __init__(self, debug=False):
        Trainer.__init__(self, debug)

    def get_data(self):
        return HomeCreditTrainer.application_train_test(self._num_rows)

    @staticmethod
    def application_train_test(num_rows=None, nan_as_category=False):
        # Read data and merge
        df = pd.read_csv(HOMECREDIT_TRAIN_CSV_FILE, nrows=num_rows)
        test_df = pd.read_csv(HOMECREDIT_TEST_CSV_FILE, nrows=num_rows)
        df = df.append(test_df).reset_index()
        # Optional: Remove 4 applications with XNA CODE_GENDER (train set)
        df = df[df["CODE_GENDER"] != "XNA"]

        # Categorical features with Binary encode (0 or 1; two categories)
        for bin_feature in ["CODE_GENDER", "FLAG_OWN_CAR", "FLAG_OWN_REALTY"]:
            df[bin_feature], uniques = pd.factorize(df[bin_feature])
        # Categorical features with One-Hot encode
        df, cat_cols = Trainer.one_hot_encoder(df, nan_as_category)

        # NaN values for DAYS_EMPLOYED: 365.243 -> nan
        df["DAYS_EMPLOYED"].replace(365243, np.nan, inplace=True)
        # Some simple new features (percentages)
        df["DAYS_EMPLOYED_PERC"] = df["DAYS_EMPLOYED"] / df["DAYS_BIRTH"]
        df["INCOME_CREDIT_PERC"] = df["AMT_INCOME_TOTAL"] / df["AMT_CREDIT"]
        df["INCOME_PER_PERSON"] = (
            df["AMT_INCOME_TOTAL"] / df["CNT_FAM_MEMBERS"]
        )
        df["ANNUITY_INCOME_PERC"] = df["AMT_ANNUITY"] / df["AMT_INCOME_TOTAL"]
        df["PAYMENT_RATE"] = df["AMT_ANNUITY"] / df["AMT_CREDIT"]
        del test_df
        gc.collect()
        return df


class JoeTrainer(Trainer):
    def __init__(self, debug=False, n_jobs=8):
        Trainer.__init__(self, debug, n_jobs)

    def get_data(self):
        # Read data
        df = pd.read_csv(JOE_TRAIN_CSV_FILE, nrows=self._num_rows)

        """# Categorical features with Binary encode
        for bin_feature in [
            "f01_mem_gender",
            "activate_flag",
            "unsub_flag",
            "redeem_flag",
        ]:
            df[bin_feature] = pd.factorize(df[bin_feature])"""
        # Categorical features with One-Hot encode
        df, cat_cols = Trainer.one_hot_encoder(df, False)
        df.rename(columns={"open_flag": "TARGET"}, inplace=True)
        # Set 20% of data as test set
        sample = df.sample(frac=0.2, random_state=1023)
        df.loc[sample.index, "TARGET"] = None
        gc.collect()
        return df


if __name__ == "__main__":
    trainer = JoeTrainer()
    trainer.train({"max_depth": 2})
